import socket

# 请将要传输的ASCII字符写到文件："F:\\test\\client-send.txt"
str = ''
file = open("F:\\test\\client-send.txt","r")  # 读取
content = file.readlines()
for item in content:
    str += item
print("读取到的内容：\n",str)
file.close()

unknown = ''                         # 加密
for i in range(len(str)):
    if ord(str[i]) < 125 and str[i] != '\n':
        temp = chr(ord(str[i]) + 1)
        unknown += temp+' '
    else:
        unknown += str[i]+' '

s = socket.socket(socket.AF_INET,socket.SOCK_STREAM)  # 发送
s.connect(('127.0.0.1',8001))
s.sendall(unknown.encode())
print("已发送加密后的内容")
print(unknown)
s.close()








