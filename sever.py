import socket

def decode(unknown):
    """
    :param unknown: 未知字符，用来拿去解密
    :return: known，已经完成解密的字符
    """
    known = ''
    for i in range(len(unknown)-1):
        if unknown[i] == '\n':
            known += '\n'
        elif ord(unknown[i]) <= 125:
            known += chr(ord(unknown[i])-1)
    return known

s = socket.socket(socket.AF_INET,socket.SOCK_STREAM)
s.bind(('127.0.0.1',8001))
s.listen()
conn, address = s.accept()
unknown = conn.recv(1024)
file = open("F:\\test\\server-receive.txt","w")
unknown = unknown.decode()
print("收到密文，正在解码中.....")
unknown = unknown.split(' ')
known = decode(unknown)

file.write(known)
print("已将收到的内容写到\"F:\\test\\server-receive.txt\"中，请注意查收！")
file.close()
s.close()

